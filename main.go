package main

import (
	"lib_bot/config"

	"fmt"
)

func main() {
	config := config.New()

	// tgClient = telegram.New()
	// fetcher := fetcher.New()
	// processor := processor.New()
	//consumer.Start(fetcher, processor)
	fmt.Printf("TG_TOKEN = %v\nTG_HOST = %v\n", config.TG_TOKEN, config.TG_HOST)
}
