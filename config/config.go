package config

import (
	"lib_bot/lib/e"

	"fmt"
	"reflect"

	"log"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	TG_TOKEN string
	TG_HOST  string
	// DB_NAME string
	// DB_HOST string
	// DB_PORT int
	// DB_PASSWORD string
}

const (
	envErr = "can't read env file"
)

func New() *Config {
	config, err := newConfigFromEnv()
	if err != nil {
		log.Fatalf(err.Error())
	}
	return config
}

// setFieldsFromEnv sets env values to Config by Config fields names dynamically
func (c *Config) setFieldsFromEnv() error {
	v := reflect.ValueOf(c).Elem()

	for i := 0; i < v.NumField(); i++ {
		fieldName := v.Type().Field(i).Name

		envValue, err := getEnv(fieldName)
		if err != nil {
			return err
		}

		v.FieldByName(fieldName).Set(reflect.ValueOf(envValue))
	}
	return nil
}

func newConfigFromEnv() (config *Config, err error) {
	defer func() { err = e.WrapIfErr(envErr, err) }()

	err = godotenv.Load(".env")
	if err != nil {
		return nil, err
	}

	config = new(Config)
	err = config.setFieldsFromEnv()
	if err != nil {
		return nil, err
	}

	return config, nil
}

func getEnv(key string) (string, error) {
	val := os.Getenv(key)
	if val == "" {
		return "", fmt.Errorf("can't get value %v from env", key)
	}
	return val, nil
}
